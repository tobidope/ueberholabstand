#!/bin/bash

#Konstanten
bildbreite=1920
bildhoehe=1080
markierung=12 # 15 auf Autobahnen, 12 sonst
scale=2

#Standardwerte (von Eingabeparametern überschrieben)
fahrstreifen=${1:-326} # gemessen zwischen den äußeren Markierungen bis zur Mitte des Mittelstreifens
fahrradbreite=${2:-65}
typ=${3:-0} # 0 = Auto, 1 = SUV, 2 = LKW
mittelstreifen=${4:-1} # 0 = nein, 1 = ja
autoabstand=${5:-200} # 150 innerorts, 200 außerorts
stvo=${6:-1} # Abstand zum rechten Rand nach StVO oder Pforzheimer Norm?
fahrradabstand=${7:-75} # gemessen vom linken Rand der rechten Markierung zur Mitte des Fahrrads
seitenstreifen=${8:-50} # gemessen vom Asphaltrand bis zum inneren Rand der Markierung
mensch=${9:-1} # leeres Fahrrad oder mit Mensch? 0 = leer, 1 = Radfahrender

filename="Abstand_"$fahrstreifen"_"$fahrradbreite"_"

if [ "$1" == "" ] ; then
  echo $0" hat neun erlaubte Parameter, die alle ganzzahlig sind. Im einzelnen:"
  echo
  echo "Fahrstreifenbreite (Default 326 cm)"
  echo "Fahrradbreite (Default 65 cm)"
  echo "Typ des überholenden Fahrzeugs: 0 Auto, 1 SUV, 2 LKW (Default 0)"
  echo "Mittelstreifen vorhanden? 0=nein, 1=ja (wenn nein, ist die Fahrstreifenbreite die Gesamtbreite) (Default 1)"
  echo "Abstand des Autos (Vorschrift: 200 cm außerorts und bei Kindern, 150 cm innerorts) (Default 200 cm)"
  echo "Wie wird der Abstand des Fahrrads zum Fahrbahnrand gemessen? 1=StVO, 0= Pforzheimer Bußgeldstelle (Default 1)"
  echo "Abstand des Fahrrads von der rechten Fahrstreifenbegrenzung (Default 75)"
  echo "Seitenstreifenbreite (Breite der Asphaltierung bis zum inneren Rand der Fahrstreifenmarkierung (Default 50)"
  echo "radfahrede Person oder leeres Fahrrad (1=Person, 0=leer) (Default 1)"
  echo
  echo "Für mehr Details: \""$0" h\" aufrufen"
  echo
  echo "ESC zum Abbrechen, (fast) beliebige andere Taste zum Fortsetzen mit Defaultwerten"
  while read -r -n1 key
  do
    if [[ $key == $'\e' ]]; then
      exit 0
    else
      break
    fi
  done
elif [ "$1" == "h" ] ; then
  echo "Kommandozeilenparameter: "
  echo "1. Fahrstreifenbreite¹"
  echo "gemessen vom inneren Rand der äußeren Fahrstreifenbegrenzung bzw. vom Bordstein"
  echo "bis zur Mitte der Mittellinie. Ist keine Mittellinie vorhanden (vierter Parameter 0),"
  echo "bezeichnet dieser Wert die Breite der Straße zwischen den Bordsteinen bzw. den inneren"
  echo "Rändern der Fahrstreifenbegrenzungen. Defaultwert: 326 cm."
  echo "2. Fahrradbreite²"
  echo "Die Gesamtbreite des Fahrrads von Lenkerende zu Lenkerende. Grafisch wird das Fahrrad-Bild"
  echo "einfach in x-Richtung verzerrt (ja, könnte man bestimmt schöner machen). Defaultwert: 65 cm."
  echo "3. Typ des überholenden Fahrzeugs"
  echo "Da man schlecht mal eben nachmessen kann, wie breit das Auto eben war, gibt es hier drei"
  echo "standardisierte Fahrzeuge."
  echo "0 bezeichnet ein Auto. Laut Bußgeldstelle Pforzheim ist ein Auto ca. 180 cm breit³, daher"
  echo "  wurde hier ein Mini Clubman angenommen. Der ist ohne Spiegel 180 cm, mit Spiegeln 203 cm breit⁴."
  echo "1 bezeichnet einen SUV. Als Beispiel habe ich einen Audi Q7 genommen: ohne Spiegel 197 cm,"
  echo "  mit Spiegeln 2,22 m breit⁴".
  echo "2 bezeichnet einen LKW mit maximalen Abmessungen: 255 cm Fahrzeugbreite⁵, mit Spiegeln 300 cm⁶."
  echo "  Kühlfahrzeuge dürfen sogar 260 cm breit sein - hier nicht berücksichtigt."
  echo "Alle drei Fahrzeuge haben die proportional richtige Höhe in der grafischen Darstellung."
  echo "Defaultwert: 0 (Auto)."
  echo "4. Mittelstreifen vorhanden?"
  echo "0 bedeutet nein, 1 steht für ja. Wirkt sich auf die Behandlung des ersten Parameters"
  echo "\"Fahrstreifenbreite\" aus - siehe dort. Defaultwert: 1 (mit Mittelstreifen)."
  echo "5. Abstand des Autos vom Fahrrad"
  echo "wird gemessen vom Lenkerende bis zur Außenspiegelkante. Ist die Straße zu schmal, wird das"
  echo "überholende Fahrzeug so weit wie möglich nach links gesetzt (Außenspiegelkante auf Fahr-"
  echo "streifenbegrenzung), rot eingefärbt und der tatsächlich Abstand ausgegeben."
  echo "Vorgeschriebene Abstände sind 200 cm außerorts und 150 cm innerorts. Handelt es sich bei den"
  echo "radfahrenden Personen um Kinder, ist auch innerorts der vorgeschriebene Überholabstand 200 cm."
  echo "Defaultwert: 200 cm (wie außerorts vorgeschrieben)."
  echo "6. Art der Bestimmung des Abstands des Fahrrads von der rechten Fahrstreifenbegrenzung³"
  echo "Die StVO schreibt hier 75 cm vor. Die Bußgeldstelle Pforzheim ist aber der Ansicht, dass die"
  echo "Fahrspur des Rads (also die Mitte der Reifen) 75 cm von der Fahrstreifenbegrenzung sein sollte."
  echo "Beide Messmethoden sind einstellbar, nach StVO: 1, nach Pforzheimer Bußgeldstelle: 0."
  echo "Defaultwert: 1 (nach StVO)."
  echo "7. Abstand des Fahrrads zum Seitenstreifen"
  echo "Die Art der Abstandsbestimmung wird in 6. festgelegt - hier kommt der Wert."
  echo "Defaultwert: 75 cm."
  echo "8. Breite des Seitenstreifens"
  echo "Gemäß den Richtlinien für das Anlegen von Landstraßen⁷ endet die Asphaltdecke nicht direkt am"
  echo "Begrenzugsstrich, sondern etwas weiter außen. Die Breite dieses Seitenstreifens geht also vom"
  echo "Ende der Asphaltdecke bis zum inneren Rand der Begrenzungslinie. Ist dieser Wert 0, so wird"
  echo "direkt an die Fahrspur angrenzend ein Bordstein gezeichnet. (ja, man könnte noch einen Rinnstein"
  echo "vorsehen…)"
  echo "Defaultwert: 50 cm (entsprechend den aktuellen Richtlinien, bis Mai 2013: 25 cm)."
  echo "9. radfahrende Person oder leeres Fahrrad"
  echo "rein kosmetische Option. 0 wählt ein leeres Fahrrad, wie es auf offiziellen Verkehrszeichen⁸"
  echo "zu sehen ist. 1 wählt eine radfahrende Person⁹."
  echo 
  echo "Quellen:"
  echo "¹) Defaultwert nach Messung von @natenom an der Landstraße L574,"
  echo "   https://twitter.com/Natenom/status/1286643200208384000"
  echo "   Er misst 3,20 zwischen den Linien, laut ⁷) ist eine Linie 12 cm breit."
  echo "²) Mein Lenker ist 65 cm breit. Ob meine Ellbogen oder so weiter rausstehen,"
  echo "   weiß ich nicht..."
  echo "³) aus einem Schriftwechsel mit @natenom, siehe"
  echo "   https://blog.natenom.com/2020/07/bussgeldstelle-pforzheim-berechnet-abstaende-ab-mitte-des-radfahrenden/"
  echo "   Zitat:"
  echo "     \"außerorts (z.B. bei einer durchschnittlichen Fahrbahnbreite von 3,75 m):"
  echo "      Abstand zur weißen Linie rechts: 0,75 m"
  echo "      Den Abstand nach rechts rechne ich ab den Rädern, es ist nicht nachzuvollziehen,"
  echo "      hierfür die rechte Außenkante des Lenkers heranzuziehen.\""
  echo "   Lediglich innerorts neben geparkten Autos berechnet die Bußgeldstelle Pforzheim den Seitenabstand"
  echo "   von in diesem Fall 1,20 m ab dem rechten Lenkerende."
  echo "   Daraus resultiert diese Aussage:"
  echo "     \"Wenn ein Fahrzeug mittig auf der Mittellinie überholt, liegt ein geringfügiger Verstoß vor."
  echo "      Bei einer durchschnittlichen Fahrzeugbreite von 1,80 m ist es 0,90 m auf der rechten Fahrbahnseite.\""
  echo "⁴) https://assets.adac.de/image/upload/v1573473524/ADAC-eV/KOR/Text/PDF/Fahrzeugbreiten_hgw9tn.pdf"
  echo "⁵) https://de.wikipedia.org/wiki/Nutzfahrzeug/Ma%C3%9Fe_und_Gewichte"
  echo "⁶) ich habe auf Twitter gefragt, wie weit Außenspiegel rausstehen, weil ich keine Quelle finden"
  echo "   konnte. @schlabonski war so freundlich, mir zu antworten: ca. 25 cm rechts und links"
  echo "   (https://twitter.com/schlabonski/status/1287010147756367873). Damit wäre die Gesamtbreite mit"
  echo "   Spiegeln 305 cm. Da es aber Straßen geben darf, die nur je 300 cm breite Fahrspuren haben (siehe"
  echo "   ⁷)), habe ich das eigenmächtig auf 300 cm reduziert."
  echo "⁷) früher https://de.wikipedia.org/wiki/Richtlinien_f%C3%BCr_die_Anlage_von_Stra%C3%9Fen_%E2%80%93_Querschnitt,"
  echo "   aktuell https://www.bast.de/BASt_2017/DE/Verkehrstechnik/Fachthemen/v1-strassentypen.html"
  echo "⁸) https://de.wikipedia.org/wiki/Bildtafel_der_Verkehrszeichen_in_der_Bundesrepublik_Deutschland_seit_2017,"
  echo "   Zeichen 277.1. Von hier habe ich auch die Auto-Grafik geholt, selbst um Rückspiegel erweitert."
  echo "   und in Höhe und Breite so verzerrt, dass das Auto- und das SUV-Bild entstanden."
  echo "   Das LKW-Bild stammt vom Zeichen 277 auf der selben Seite, Spiegel ebenfalls von mir."
  echo "⁹) Das Bild der radfahrenden Person hat mir @lebenswerteCity zur Verfügung gestellt - es ist das SVG, das"
  echo "   https://twitter.com/lebenswerteCity/status/1290250052703645698 zugrunde liegt."
  echo
  echo "Mein Dank gilt @natenom, @schlabonski und @lebenswerteCity, die mir mit Antworten, Nachdenkhilfen,"
  echo "Ansporn und grafischen Fähigkeiten geholfen haben."
  echo "Es gibt noch massenweise Möglichkeiten, was zu verbessern - nur zu, wenn ihr wollt und könnt - oder"
  echo "schreibt mir Anregungen auf Twitter an @joschtl oder per Mail an radwrdlabstand.20.pcb@a-bc.net."
  echo
  exit 0
fi

#Berechnetes
fahrradbreitepx=$(($scale*$fahrradbreite))
if [ "$typ" -eq "2" ] ; then
  autobreite=300
  autobreitepx=$(($scale*$autobreite))
  autohoehepx=$(($autobreitepx*4000/3000)) # 2,55 m ohne, 3,00 mit Spiegeln, 4 m hoch
  autobildschwarz="lkw_mit.png"
  autobildrot="lkw_mit_rot.png"
  filename=$filename"LKW_"
elif [ "$typ" -eq "1" ] ; then
  autobreite=222
  autobreitepx=$(($scale*$autobreite))
  autohoehepx=$(($autobreitepx*1740/2220)) # Audi Q7: 1,97 m ohne, 2,22 m mit Spiegeln, 1,74 hoch
  autobildschwarz="suv_mit.png"
  autobildrot="suv_mit_rot.png"
  filename=$filename"SUV_"
else
  autobreite=203
  autobreitepx=$(($scale*$autobreite))
  autohoehepx=$(($autobreitepx*1441/2030)) # Mini Clubman: 1,80 m ohne, 2,03 m mit Spiegeln, 1,441 m hoch
  autobildschwarz="auto_mit.png"
  autobildrot="auto_mit_rot.png"
  filename=$filename"Auto_"
fi

seitenstreifenpx=$(($scale*$seitenstreifen))
markierungpx=$(($scale*$markierung))
fahrstreifenpx=$(($scale*$fahrstreifen))
mitte=$((bildbreite/2))
fahrradabstandpx=$(($scale*$fahrradabstand))
autoabstandpx=$(($scale*$autoabstand))
if [ "$mittelstreifen" -eq "1" ] ; then
  asphaltlinks=$(($mitte-$fahrstreifenpx-$seitenstreifenpx))
  asphaltrechts=$(($mitte+$fahrstreifenpx+$seitenstreifenpx))
  filename=$filename"mit_"$autoabstand"_"
else
  asphaltlinks=$(($mitte-$fahrstreifenpx/2-$seitenstreifenpx))
  asphaltrechts=$(($mitte+$fahrstreifenpx/2+$seitenstreifenpx))
  filename=$filename"ohne_"$autoabstand"_"
fi
linkslinks=$(($asphaltlinks+$seitenstreifenpx-$markierungpx))
linksrechts=$(($asphaltlinks+$seitenstreifenpx))
rechtslinks=$(($asphaltrechts-$seitenstreifenpx))
rechtsrechts=$(($asphaltrechts-$seitenstreifenpx+$markierungpx))
mittelinks=$(($mitte-$markierungpx/2))
mitterechts=$(($mitte+$markierungpx/2))
if [ "$stvo" -eq "0" ] ; then
  fahrradmitte=$(($rechtslinks-$fahrradabstandpx))
  fahrradlinks=$(($fahrradmitte-$fahrradbreitepx/2))
  fahrradrechts=$(($fahrradmitte+$fahrradbreitepx/2))
  filename=$filename"PF_"$fahrradabstand"_"$seitenstreifen"_"
else
  fahrradrechts=$(($rechtslinks-$fahrradabstandpx))
  fahrradmitte=$(($fahrradrechts-$fahrradbreitepx/2))
  fahrradlinks=$(($fahrradmitte-$fahrradbreitepx/2))
  filename=$filename"StVO_"$fahrradabstand"_"$seitenstreifen"_"
fi
if [ "$mensch" -eq "0" ] ; then
  fahrradhoehepx=$(($scale*122))
  fahrradbild="fahrrad.png"
  filename=$filename"Rad.png"
else
  fahrradhoehepx=$(($scale*183))
  fahrradbild="radler.png"
  filename=$filename"RadlerIn.png"
fi
fahrradoben=$((900-$fahrradhoehepx))
autolinks=$(($fahrradlinks-$autoabstandpx-$autobreitepx))
autorechts=$(($fahrradlinks-$autoabstandpx))
if [ "$autolinks" -lt "$linksrechts" ]; then
  autolinks=$linksrechts
  autorechts=$(($autolinks+$autobreitepx))
  autoschwarz=0
  autoabstandpx=$(($fahrradlinks-$autorechts))
  autoabstand=$(($autoabstandpx/$scale))
else
  autoschwarz=1
fi
autooben=$((900-$autohoehepx))
seitenstreifentextlinks=$(($asphaltlinks+$seitenstreifenpx/2-8))
seitenstreifentextrechts=$(($asphaltrechts-$seitenstreifenpx/2-8))
fahrstreifentextlinks=$(($asphaltlinks+$seitenstreifenpx+$fahrstreifenpx/2-8))
fahrstreifentextrechts=$(($asphaltrechts-$seitenstreifenpx-$fahrstreifenpx/2-8))
autoabstandtext=$((($fahrradlinks+$autorechts)/2-8))
if [ "$stvo" -eq "0" ] ; then
  fahrradabstandtext=$((($rechtslinks+$fahrradmitte)/2-8))
else
  fahrradabstandtext=$((($rechtslinks+$fahrradrechts)/2-8))
fi

#line1="Fahrstreifenbreite: $fahrstreifen cm"
#line2="Autobreite mit Spiegel: $autobreite cm"
if [ "$typ" -eq "2" ] ; then
  line2="LKW mit Maximalmaßen: Breite 2,55 m ohne, 3,00 m mit Spiegeln; Höhe 4,00 m"
elif [ "$typ" -eq "1" ] ; then
  line2="typischer SUV: Audi Q7. Breite 1,968 m ohne, 2,22 m mit Spiegeln; Höhe 1,74 m"
else
  line2="typisches Auto: Mini Clubman. Breite 1,80 m ohne, 2,03 m mit Spiegeln; Höhe 1,441 m"
  line3="(ausgewählt wegen der Aussage der Bußgeldstelle Pforzheim, dass ein Auto 1,80 m breit sei)"
fi
#line3="Überholabstand (Spiegel zu linkem Lenkerende): $autoabstand cm"
line4="Fahrradbreite: $fahrradbreite cm"
#line5="Abstand Fahrradmitte zum linken Rand der Fahrbahnbegrenzungsmarkierung: $fahrradabstand cm"
# -draw "text 200,140 'Autobreite ohne Spiegel: 1,80 m; mit Spiegel: 2,03 m (entspricht Mini Clubman)'"

cs="convert -size "$bildbreite"x"$bildhoehe" canvas:darkgray "
cs=$cs"-fill black -strokewidth 0 -stroke none "
cs=$cs"-pointsize 60 "
cs=$cs"-annotate +770+70 \"Überholabstand\" "
cs=$cs"-pointsize 18 "
cs=$cs"-annotate +1000+120 \"$line1\" "
cs=$cs"-annotate +1000+140 \"$line2\" "
cs=$cs"-annotate +1000+160 \"$line3\" "
cs=$cs"-annotate +1000+180 \"$line4\" "
cs=$cs"-annotate +1000+200 \"$line5\" "
cs=$cs"-stroke black -fill black -strokewidth 0 "
cs=$cs"-draw \"rectangle $asphaltlinks,900,$asphaltrechts,920\" "
cs=$cs"-stroke white -fill white "
if [ "$mittelstreifen" -eq "1" ] ; then
  cs=$cs"-draw \"rectangle $mittelinks,895,$mitterechts,910\" "
fi
if [ "$seitenstreifen" -gt "0" ] ; then
  cs=$cs"-draw \"rectangle $linkslinks,895,$linksrechts,910\" "
  cs=$cs"-draw \"rectangle $rechtslinks,895,$rechtsrechts,910\" "
else
  cs=$cs"-stroke '#505050' -fill '#505050' "
  cs=$cs"-draw \"rectangle \"$(($asphaltlinks-20))\",920 \"$(($asphaltlinks-50))\",860\" "
  cs=$cs"-draw \"rectangle \"$asphaltlinks\",920 \"$(($asphaltlinks-50))\",880\" "
  cs=$cs"-draw \"circle \"$(($asphaltlinks-20))\",880 $asphaltlinks,880\" "
  cs=$cs"-draw \"rectangle \"$(($asphaltrechts+20))\",920 \"$(($asphaltrechts+50))\",860\" "
  cs=$cs"-draw \"rectangle \"$asphaltrechts\",920 \"$(($asphaltrechts+50))\",880\" "
  cs=$cs"-draw \"circle \"$(($asphaltrechts+20))\",880 $asphaltrechts,880\" "
fi
# zum Debuggen... cs=$cs"-annotate +200+300 \"$fahrradlinks,$fahrradmitte,$fahrradrechts,$fahrradbreitepx\" "
cs=$cs"-draw \"image SrcOver $fahrradlinks,$fahrradoben $fahrradbreitepx,$fahrradhoehepx '$fahrradbild'\" "
if [ "$autoschwarz" -eq "1" ]; then
  cs=$cs"-draw \"image SrcOver $autolinks,$autooben $autobreitepx,$autohoehepx '$autobildschwarz'\" "
else
  cs=$cs"-draw \"image SrcOver $autolinks,$autooben $autobreitepx,$autohoehepx '$autobildrot'\" "
  cs=$cs"-stroke black -fill none -strokewidth 1 "
  cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $linksrechts,900 M $linksrechts,$autooben'\" "
fi
cs=$cs"-stroke black -fill none -strokewidth 1 "
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $asphaltlinks,920 M $asphaltlinks,1020'\" "
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $asphaltrechts,920 L $asphaltrechts,1020'\" "
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $linksrechts,920 L $linksrechts,1020'\" "
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $rechtslinks,920 L $rechtslinks,1020'\" "
if [ "$mittelstreifen" -eq "1" ] ; then
  cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $mitte,920 L $mitte,1020'\" "
fi
if [ "$stvo" -eq "0" ] ; then
  cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $fahrradmitte,900 L $fahrradmitte,970'\" "
else
  cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $fahrradrechts,$fahrradoben L $fahrradrechts,970'\" "
fi
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $fahrradlinks,970 L $fahrradlinks,$fahrradoben'\" "
cs=$cs"-draw \"stroke-dasharray 3 5 path 'M $autorechts,970 L $autorechts,$autooben'\" "

cs=$cs"-draw \"stroke-dasharray 2 2 path 'M $asphaltlinks,1020 L $asphaltrechts,1020'\" "
if [ "$stvo" -eq "0" ] ; then
  cs=$cs"-draw \"stroke-dasharray 2 2 path 'M $fahrradmitte,970 L $rechtslinks,970'\" "
else
  cs=$cs"-draw \"stroke-dasharray 2 2 path 'M $fahrradrechts,970 L $rechtslinks,970'\" "
fi
cs=$cs"-draw \"stroke-dasharray 2 2 path 'M $fahrradlinks,970 L $autorechts,970'\" "
cs=$cs"-pointsize 18 -fill black -stroke none "
#dringend ändern
if [ "$seitenstreifen" -gt "12" ] ; then
  cs=$cs"-draw \"text $seitenstreifentextlinks,1010 '$seitenstreifen'\" "
  cs=$cs"-draw \"text $seitenstreifentextrechts,1010 '$seitenstreifen'\" "
fi
cs=$cs"-draw \"text $fahrstreifentextlinks,1010 '$fahrstreifen'\" "
if [ "$mittelstreifen" -eq "1" ] ; then
  cs=$cs"-draw \"text $fahrstreifentextrechts,1010 '$fahrstreifen'\" "
fi
cs=$cs"-draw \"text $fahrradabstandtext,960 '$fahrradabstand'\" "
if [ "$autoschwarz" -eq "0" ] ; then
  cs=$cs"-fill red -stroke red "
fi
cs=$cs"-draw \"text $autoabstandtext,960 '$autoabstand'\" "
#bis hier
cs=$cs$filename
echo $cs > temp.sh
bash temp.sh
rm temp.sh
